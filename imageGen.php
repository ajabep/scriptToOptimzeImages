<?php

/**
 * Resize and optimise an image and cache it
 * Nowly, works only with JPEG images.
 * 
 * The URL to access to this file must be like 
 * 	`/img/{{ name of original file }}.{{ extention }}`
 *  or  `/img/{{ name of original file }}.{{ Number used to a good cache management}}.{{ extention }}`
 *  or  `/img/{{ name of original file }}-{{ Width wanted }}.{{ extention }}`
 *  or  `/img/{{ name of original file }}-{{ Width wanted }}.{{ Number used to a good cache management}}.{{ extention }}`
 * 
 * Where :
 *  {{ name of original file }} is the name, but the extention, of the original file wanted.
 *    The file must be in the `original` folder.
 *    The name must be composed of chars `0` to `9`, `a` to `z` and `A` ro `Z`.
 *  {{ width wanted }} is the width wanted if you want a resize. Else, there is no resize.
 *  {{ Number used to a good cache management }} is a prefix which permit to give to your static files the max age possible. Offen used to optimize the cache.
 *  {{ extention }} is the exetention of the file. Can be every thing, but the return content will ALWAYS be a JPEG image.
 * 
 * The original files must be placed in the `original` folder.
 * 
 * 
 * 
 * Requirements :
 * 
 *  * The GD Library or the Imagick librairy
 * 
 * 
 * @author ajabep
 **/




$matches = array();

$findPatern = preg_match('#/img/([0-9a-zA-Z]+)(-([0-9]+))?\.(([0-9]+)\.)?([a-zA-Z]+)$#', $_SERVER['REQUEST_URI'], $matches);
// $matches :
//  [1] : name of original file
//  [2] : `-{{ width wanted }}`
//  [3] : the width wanted
//  [4] : `{{ Number used to a good cache management }}.`
//  [5] : the number used to a good cache management
//  [6] : extention

if (!count($matches)) {
	gen404();
}

$matches[2] = $matches[1] . $matches[2];


$path2origin = 'original/' . $matches[1] . '.jpeg';
$path2final = $matches[2] . '.' . $matches[6];


function sendImg($pathToFile) {
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($pathToFile)) . ' GMT');
	header('Date: ' . gmdate('M d Y H:i:s') . ' GMT');
	header('Content-Type: image/jpeg');
	readfile($pathToFile);
	exit;
}


// Will throw a HTTP 404 error
function gen404() {
	header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', 404, true);
	echo ('404 File Not Found');
	exit;
}

if (!$findPatern || !file_exists($path2origin)) {
	gen404();
}


if (file_exists($path2final))
	sendImg($path2final);


if (class_exists('Imagick')) {
	// Imagik supproté

	$image = new Imagick($path2origin);

	if (!empty($matches[3])) {
		$width = $image->getImageWidth();

		if ($width > $matches[3])
			$image->thumbnailImage($matches[3], 0);
	}

	$image->setImageFormat("jpg");
	$image->writeImage($matches[2] . $matches[6]);
}

elseif (function_exists('gd_info') && ((!empty(gd_info()['JPEG Support']) && gd_info()['JPEG Support']) || (!empty(gd_info()['JPG Support']) && gd_info()['JPG Support']))) {
	// GD supproté avec JPEG

	$originImage = imagecreatefromjpeg($path2origin);

	$infoImgOrigin = getimagesize($path2origin);

	if (empty($matches[3]))
		$matches[3] = $infoImgOrigin[0];


	$height = ($matches[3] * $infoImgOrigin[1]) / $infoImgOrigin[0]; // height wanted = (width wanted * original height) / original width

	if ($infoImgOrigin[0] <= $matches[3]) {
		$matches[3] = $infoImgOrigin[0];
		$height = $infoImgOrigin[1];
	}

	$finalImg = imagecreatetruecolor($matches[3], $height);
	imagecolorallocate($finalImg, 255, 255, 255);

	imagecopyresized($finalImg, $originImage, 0, 0, 0, 0, $matches[3], $height, $infoImgOrigin[0], $infoImgOrigin[1]);

	imagedestroy($originImage);

	imagejpeg($finalImg, $path2final);
}
else
	throw new Exception('Pas implémenté');


sendImg($path2final);

